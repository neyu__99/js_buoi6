// ex1 
document.getElementById('Ex1Btn').onclick=function (){
    var n=0
    var Tong=0
    while (Tong<10000){
        n++ 
        Tong+=n
    }
    document.getElementById('Ex1Result').innerHTML=`Số nguyên dương nhỏ nhất: ${n}`
}
// ex2
document.getElementById('Ex2Btn').onclick=function (){
    var x=Number(document.getElementById('Ex2SoX').value)
    var n=Number(document.getElementById('Ex2SoN').value)
    var i=1
    var Tong=0
    while (i<=n){
        Tong+=Math.pow(x,i) 
        i++ 
    }
    document.getElementById('Ex2Result').innerHTML=`Tổng: ${Tong}`
}    
// ex3
document.getElementById('Ex3Btn').onclick=function (){
    var n=Number(document.getElementById('Ex3SoN').value)
    var i=1
    var GT=1
    while (i<=n){
        GT=GT*i 
        i++
    }
    document.getElementById('Ex3Result').innerHTML=`Giai thừa: ${GT}`
}  
// ex4
document.getElementById('Ex4Btn').onclick=function (){
    var text=document.getElementById('Ex4Result')
    var i=1
    while (i<=10){
        if (i%2!=0){
            text.innerHTML+='<div class="divle p-1" style="background-color: blue; color: white;">Div lẻ</div>'
        }
        else {
            text.innerHTML+='<div class="divchan p-1" style="background-color: red; color: white;">Div chẵn</div>'
        }
        i++
    }
}


